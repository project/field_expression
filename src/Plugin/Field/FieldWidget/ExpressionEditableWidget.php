<?php

namespace Drupal\field_expression\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'expression_editable' widget.
 *
 * @FieldWidget(
 *   id = "expression_editable",
 *   label = @Translation("Expression Override"),
 *   field_types = {
 *     "expression_integer",
 *     "expression_decimal",
 *     "expression_float"
 *   }
 * )
 */
class ExpressionEditableWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = [];

    $element['value'] = $element + [
        '#title' => $this->fieldDefinition->getName(),
        '#type' => 'textfield',
        '#default_value' => isset($items[0]->value) ? $items[0]->value : NULL,
        '#description' => $this->t('Normally this field should not be shown!'),
    ];

    $element['value'] = $element + [
        '#title' => $this->fieldDefinition->getName(),
        //        '#type' => 'hidden',
        '#value' => $this->getDefaultValue(),
        '#disabled' => TRUE,
        '#description' => $this->t('Normally this field should not be shown!'),
    ];

    return $element;
  }

  /**
   * Define how the widget is constructed.
   */
  public function getDefaultValue() {
    return 0;
  }

}
