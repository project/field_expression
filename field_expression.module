<?php

/**
 * @file
 * Contains field_expression.module.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_help().
 */
function field_expression_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the field_expression module.
    case 'help.page.field_expression':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Provides a field allowing the value to be calculated using an expression containing tokens.') . '</p>';
      return $output;

    default:
  }
}



/**
 * Implements hook_form_alter().
 */
function field_expression_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  if ($form_id == 'field_storage_config_edit_form') {
    if (($field_config = $form_state->get('field_config')) && $field_config->get('field_type') == 'field_expression') {
      $form['cardinality_container']['#disabled'] = TRUE;
      unset($form['cardinality_container']['cardinality_number']['#states']);
      $form['cardinality_container']['#suffix'] = '<div class="description"><em>Expression fields are restricted to single values. Since there is only one expression per field, multiple items would just evaluate to the same value.</em></div>';
    }
  }
}
